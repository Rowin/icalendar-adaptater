<?php
error_reporting(E_ALL);

include 'caption.php';
include 'Event.php';
include 'Calendar.php';

try {
    $calendar = file_get_contents('http://aurionweb.ifma.fr/webaurion/ICS/'.htmlentities($_GET['name']).'@sigma-clermont.fr.ics');
    //include('calendar.php');
    if (!$calendar) {
        throw new Exception("Impossible de récupérer le calendrier !");
    }
    
    //header('Content-Type: text/calendar');
    //header('Content-Disposition: attachment; filename="calendar_'.$_GET['name'].'.ics"');
    
    preg_match_all("#BEGIN:VEVENT(.*?)END:VEVENT#s", $calendar, $matches);
    
    $vcalendar = new iCalendar\Calendar;
    foreach($matches[1] as $event) {
        preg_match("#DTSTART;TZID=(.*?):(\d{8}T\d{6})#", $event, $matches);
        if(isset($matches[1], $matches[2])) {
			$values['tzid'] = new DateTimeZone($matches[1]);
			$values['dtstart'] = DateTime::createFromFormat('Ymd\THis', $matches[2], $values['tzid']);
		}
			
        preg_match("#DTEND;TZID=.*?:(\d{8}T\d{6})#", $event, $matches);
        if(isset($matches[1])) {
			$values['dtend'] = DateTime::createFromFormat('Ymd\THis', $matches[1], $values['tzid']);
		}
		
        preg_match("#UID:(.*)\\n#", $event, $matches);
        if(isset($matches[1])) {
			$values['uid'] = $matches[1];
		}
		
        preg_match("#LOCATION:(.*)\\n#", $event, $matches);
        if(isset($matches[1])) {
			$values['location'] = iCalendar\location($matches[1]);
        }
        
        preg_match("#DESCRIPTION:\n(( -[^\r\n\t\f]*\\n\n)*)#", $event, $matches);
		if(isset($matches[1])) {
			$values['description'] = $matches[1];
			
			preg_match("#Cours : \d{2}_\d{2}_[A-Z]+_S\d{2}_[A-Z]+_([A-Z]+)#", $values['description'], $matches);
			$values['summary'] = iCalendar\subject($matches[1]);
			
			preg_match("#SUMMARY:(.*)\\n#", $event, $matches);
			$values['description'] .= ' - Résumé : ' . $matches[1] . "\n";
		}
			
        $vcalendar->addEvent(new iCalendar\Event($values));
    }
    
    //echo $vcalendar->getCalendar();
	
    
} catch (Exception $e) {
    header('Content-Type: text/html');
    header("HTTP/1.0 503 Service Unavailable");
    echo "Exception interceptée. Message reçu : ".$e->getMessage()."\n";
}
