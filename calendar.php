<?php
$calendar = "BEGIN:VEVENT
DTSTART;TZID=Europe/Paris:20161207T133000
DTEND;TZID=Europe/Paris:20161207T174500
DTSTAMP:20161207T174500Z
UID:201612071745201612071330448174620161108bb60f4b64d6f2e24ed8b80597be5a4b9@sigma-clermont.fr
SEQUENCE:1
SUMMARY: [TP12-halle de GP E023 Polytechhalle de GP E023 Polytechhalle de GP E023 Polytech]  CH_3A_GC_G1  CHIMIE- [Intervenant(s) : A.MARCATI / F.GROS / G.DJELVEH]\n
DESCRIPTION:
 - Cours : 16_17_CH_S09_GC_TP\n
 - Activité : Travaux Pratiques (12)\n
 - Remarque(s) : halle de GP E023 Polytechhalle de GP E023 Polytechhalle de GP E023 Polytech\n
 - Groupe(s) : 16_17_CH_3A_GC_G1  chimie 3A GC 1\n
 - Matiere :  CHIMIE\n
 - Salle(s) :  \n
END:VEVENT";
