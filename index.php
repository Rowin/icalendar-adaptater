<?php
error_reporting(E_ALL);

try {
    $calendar = file_get_contents('http://aurionweb.ifma.fr/webaurion/ICS/'.htmlentities($_GET['name']).'@sigma-clermont.fr.ics');
    
    if (!$calendar) {
        throw new Exception("Impossible de récupérer le calendrier !");
    }
    
    header('Content-Type: text/calendar');
    header('Content-Disposition: attachment; filename="calendar.ics"');
    
    function location($matches)
    {
        switch ($matches[1]) {
            case "CHM103":
                $name = "amphi Rémi";
                break;
            case "CHM111":
                $name = "salle TD 111";
                break;
            case "CHM112":
                $name = "salle TD 112";
                break;
            case "CHM113":
                $name = "salle TD 113";
                break;
            case "CHM114":
                $name = "salle TD 114";
                break;
            case "CHM210":
                $name = "Labo TP CPA";
                break;
            case "CHM219":
                $name = "Labo TP CPA";
                break;
            case "CHM223":
                $name = "salle TD 223";
                break;
            case "CHM224":
                $name = "Labo TP ORGA";
                break;
            case "CHM230":
                $name = "Labo TP ORGA";
                break;
            case "CHM232":
                $name = "Labo TP ORGA";
                break;
            case "CHM234":
                $name = "Labo TP ORGA";
                break;
            case "CHM235":
                $name = "Labo TP ORGA";
                break;
            case "CHM237":
                $name = "salle informatique orga";
                break;
            case "CHM238":
                $name = "Labo TP ORGA";
                break;
            case "CHM241":
                $name = "Labo TP CPA";
                break;
            case "CHM242":
                $name = "Labo TP CPA";
                break;
            case "CHM254":
                $name = "Labo TP ORGA";
                break;
            case "CTT_MMS301":
                $name = "Labo Laplace";
                break;
            case "CTT_MMS302":
                $name = "Labo Willis";
                break;
            case "CTT_MMS303":
                $name = "Labo Maxwell";
                break;
            case "CTT_PST302":
                $name = "Labo Galilée";
                break;
            case "CTT283":
                $name = "Labo de TP Castigliano";
                break;
            case "CTT321":
                $name = "Halle Technique IFMA";
                break;
            case "CTT321_BIS":
                $name = "1A (indispo CTT321/LP)";
                break;
            case "CTT347":
                $name = "Dynamique";
                break;
            case "CTT350":
                $name = "CTT - Grotte";
                break;
            case "CTT351":
                $name = "Salle de Montage";
                break;
            case "CTT352":
                $name = "CTT- Assemblage";
                break;
            case "CTT354":
                $name = "Salle Mécatronique";
                break;
            case "CTT354_BIS":
                $name = "Mécatronique UE AI";
                break;
            case "CTT357":
                $name = "remplace ctt_pst304";
                break;
            case "CTT401":
                $name = "Amphi Newton";
                break;
            case "CTT442":
                $name = "CAO/CFAO";
                break;
            case "CTT443":
                $name = "Labo de TP Tresca";
                break;
            case "CTT446":
                $name = "Salle TD-CTT446";
                break;
            case "CTT489":
                $name = "Amphi Lagrange";
                break;
            case "CTTFAO_MMS207":
                $name = "Salle BETTENCOURT(Labo)";
                break;
            case "CTTRA":
                $name = "CTT321 pour l'UE RA";
                break;
            case "MMS101":
                $name = "Salle FOUCAULT";
                break;
            case "MMS102":
                $name = "Salle HERTZ";
                break;
            case "MMS204":
                $name = "Salle HACHETTE(Labo)";
                break;
            case "MMS205":
                $name = "Labo de TP DIESEL";
                break;
            case "MMS206":
                $name = "Salle LANZ(Labo)";
                break;
            case "MMS401":
                $name = "Salle TD 2A-MMS1 (MMS401)";
                break;
            case "MMS402":
                $name = "Salle TD-MMS402";
                break;
            case "MMS424":
                $name = "Salle TD-MMS424";
                break;
            case "MMS452":
                $name = "Lanchester";
                break;
            case "MMS453":
                $name = "Mec@tech";
                break;
            case "POLY":
                $name = "Salle Hall Polytech";
                break;
            case "PST202":
                $name = "Salle TD 3A- ST2M2";
                break;
            case "PST203":
                $name = "Salle TD-PST203(école chimie)";
                break;
            case "PST212":
                $name = "Salle TD 3A- St2M2";
                break;
            case "PST213":
                $name = "Salle TD-PST213";
                break;
            case "PST301":
                $name = "Labo Von Karmann";
                break;
            case "PST303":
                $name = "Labo de TP TSAI";
                break;
            case "PST307":
                $name = "Labo Rayleigh";
                break;
            case "PST401":
                $name = "Labo de TP Fluides";
                break;
            case "PST451":
                $name = "Salle Bernoulli";
                break;
            case "PST455":
                $name = "Amphi Timoschenko";
                break;
            case "PST456":
                $name = "Salle TD 2A-ST2M1 (PST456)";
                break;
            case "PST457":
                $name = "Salle TD 2A-ST2M2 (PST457)";
                break;
            case "RAM002":
                $name = "salle rambaud";
                break;
            case "RAM003":
                $name = "salle verte (tic-tac)";
                break;
            case "RAM004":
                $name = "salle jaune";
                break;
            case "RAM011":
                $name = "salle multimédia";
                break;
            case "SERI":
                $name = "SERI";
                break;
            case "SPA005":
                $name = "Salle TD 2A-MMS2";
                break;
            case "SPA006":
                $name = "Salle TD 2A-MMS3 ";
                break;
            case "SPA007":
                $name = "Salle TD 2A-SIL1";
                break;
            case "SPA008":
                $name = "Salle TD 2A- ST2M1";
                break;
            case "SPA101":
                $name = "Salle du Conseil (reservé Direction)";
                break;
            case "SPA102":
                $name = "Salle de TD-SPA102 (ecole chimie)";
                break;
            case "SPA201":
                $name = "Bureaux";
                break;
            case "SPA205":
                $name = "Salle Réunion et Langues";
                break;
            case "SPA255":
                $name = "Amphi Poincaré";
                break;
            case "SPA256":
                $name = "Salle TD 3A-SIL1 ";
                break;
            case "SPA257":
                $name = "Salle TD 3A-SIL2";
                break;
            case "SPA301":
                $name = "Labo Copernic";
                break;
            case "SPA302":
                $name = "Labo Kepler";
                break;
            case "SPA303":
                $name = "Labo Coulomb";
                break;
            case "SPA455":
                $name = "Salle Sophie Germain";
                break;
            case "SPA456":
                $name = "Salle informatique (projets)";
                break;
            case "SPA457":
                $name = "Salle Taylor";
                break;
            case "TCM001":
                $name = "Amphi Marie Curie";
                break;
            case "TCM002":
                $name = "Salle TD-TCM002- (chimie)";
                break;
            case "TCM003":
                $name = "Salle TD 3A - MMS 2";
                break;
            case "TCM004":
                $name = "Salle TD-TCM004";
                break;
            case "TCM005":
                $name = "Labo Cugnot";
                break;
            case "TCM005BIS":
                $name = "Labo Navier";
                break;
            case "TCM006":
                $name = "Salle TD ";
                break;
            case "TCM007":
                $name = " Labo Ader";
                break;
            case "TCM008":
                $name = "ACCUEIL";
                break;
            case "TCM009":
                $name = "Amphi Blaise Pascal";
                break;
            case "TCM010":
                $name = "Amphi Léonard de Vinci";
                break;
            case "TCM016":
                $name = "Salle TD 3A-MMS 1";
                break;
            case "TCM101":
                $name = "Salle Info Léopold";
                break;
            case "TCM102":
                $name = "Salle Info Archimède";
                break;
            case "TCM103":
                $name = "Salle Info Descartes";
                break;
            case "TCM104":
                $name = "Salle Info Borges";
                break;
            case "TCM105":
                $name = "Amphi Dante";
                break;
            case "TCM201":
                $name = "Salle TD 1A G1 - TCM201";
                break;
            case "TCM202":
                $name = "Salle TD 1A G2 - TCM202";
                break;
            case "TCM203":
                $name = "Salle TD 1A G3 - TCM203";
                break;
            case "TCM204":
                $name = "Salle apprentis 1A et 2A";
                break;
            case "TCM205":
                $name = "Salle apprentis 3A";
                break;
            case "TCM301":
                $name = "Salle TD 1A G4 - TCM301";
                break;
            case "TCM302":
                $name = "Salle TD 1A G5 - TCM302";
                break;
            case "TCM303":
                $name = "Salle TD 1A G6 - TCM303";
                break;
            case "TCM304":
                $name = "Salle TD 1A G7 - TCM304";
                break;
            case "TCM306":
                $name = "Salle TD 3A- MMS3";
                break;
            case "TCM308":
                $name = "Salle TD VISIO CONF";
                break;
            case "VES008":
                $name = "salle des actes";
                break;
            case "VES104":
                $name = "salle informatique";
                break;
            case "VES105":
                $name = "Salle de TP analyses Miné";
                break;
            case "VES107":
                $name = "salle TP chimie 2 miné";
                break;
            case "VES108":
                $name = "salle TP chimie 5 miné";
                break;
            case "VES109":
                $name = "salle TP chimie 4B miné";
                break;
            case "VES110":
                $name = "salle TP chimie 4A miné";
                break;
            case "VES112":
                $name = "salle TP chimie 1 bibliothèque miné";
                break;
            case "VES115":
                $name = "salle TP synthese inorganique miné";
                break;
            default:
                $name = $matches[2];
        }
        return "LOCATION:".$name;
    }

    function subject($matches)
    {
        switch ($matches[2]) {
            case "TOEIC":
                $name = "TOEIC";
                break;
            case "ANG":
                $name = "Anglais";
                break;
            case "TS":
                $name = "Techniques de Séparation Membranaires et solides";
                break;
            case "BTAP":
                $name = "Bilans Transferts et analyse des procédés";
                break;
            case "ELL":
                $name = "Extraction Liquide-liquide";
                break;
            case "CONM":
                $name = "Compléments Outils numériques / MATLAB";
                break;
            case "DP":
                $name = "Développement Personnel";
                break;
            case "PRO":
                $name = "PROPHY PROSIM";
                break;
            case "PPTT":
                $name = "Prévision de propriétés thermodynamiques et de transport";
                break;
            default:
                $name = $matches[2];
        }
        return 'Cours : '.$matches[1].' - '.$name;
    }

    $calendar = preg_replace_callback("#LOCATION:([a-zA-Z0-9]{6})#", "location", $calendar);
    $calendar = preg_replace_callback("#Cours : [0-9]{2}_[0-9]{2}_CH_S[0-9]{2}_([A-Z]+)_([A-Z]+)#", "subject", $calendar);
    
    if (false) {
        $event = "BEGIN:VEVENT
DTSTART;TZID=Europe/Paris:20160926T060000
DTEND;TZID=Europe/Paris:20160926T080000
DTSTAMP:20161011T100000Z
UID:2016101110002016101108004445443201609227a1c751596731e350ada2a79797857ff@sigma-clermont.fr
SEQUENCE:1
LOCATION:dans ton....\n
SUMMARY: HAHAHA JE VOUS AI BIEN NIQUÉ [Intervenant: Zangdar]\n
DESCRIPTION:
 - Description: :3
END:VEVENT
END:VCALENDAR";
    
        $calendar = preg_replace("#END:VCALENDAR#", $event, $calendar);
    }
    
    echo $calendar;
} catch (Exception $e) {
    header('Content-Type: text/html');
    header("HTTP/1.0 503 Service Unavailable");
    echo "Exception interceptée. Message reçu : ".$e->getMessage()."\n";
}
