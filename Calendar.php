<?php
namespace iCalendar;

class Calendar
{
    private $m_events;
    
    public function addEvent(Event $event)
    {
        $this->m_events[] = $event;
    }
    
    public function getCalendar()
    {
        $calendar = "BEGIN:VCALENDAR\n";
        
        foreach ($this->m_events as $event) {
            $calendar .= $event->getEvent();
        }
        
        $calendar .= "END:VCALENDAR\n";
        
        return $calendar;
    }
}
