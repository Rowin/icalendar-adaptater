<?php
namespace iCalendar;

class Event
{
    private $m_dtstart;
    private $m_dtend;
    private $m_dtstamp;
    private $m_summary;
    private $m_location;
    private $m_categories;
    private $m_status;
    private $m_description;
    private $m_transp;
    private $m_sequence;
    private $m_uid;
    private $m_tzid;
    
    public function __construct(array $values)
    {
        $default_values = array(
            'dtstart'       => new \DateTime(),
            'dtend'         => new \DateTime(),
            'dtstamp'       => new \DateTime(),
            'summary'       => null,
            'location'      => null,
            'categories'    => null,
            'status'        => null,
            'description'   => null,
            'transp'        => null,
            'sequence'      => null,
            'tzid'          => new \DateTimeZone("Europe/Paris"),
            'uid'           => null,
        );
        
        $values = array_merge($default_values, $values);
        
        $this->m_dtstart        = $values['dtstart'];
        $this->m_dtend          = $values['dtend'];
        $this->m_dtstamp        = $values['dtstamp'];
        $this->m_summary        = $values['summary'];
        $this->m_location       = $values['location'];
        $this->m_categories     = $values['categories'];
        $this->m_status         = $values['status'];
        $this->m_description    = $values['description'];
        $this->m_transp         = $values['transp'];
        $this->m_sequence       = $values['sequence'];
        $this->m_tzid           = $values['tzid'];
        $this->m_uid            = $values['uid'];
    }
    
    public function getEvent()
    {
        $event  = "BEGIN:VEVENT\n";
        $event .= "DTSTART;TZID=".$this->getTimeZone().":".$this->getStartTime()."\n";
        $event .= "DTEND;TZID=".$this->getTimeZone().":".$this->getEndTime()."\n";
        $event .= "DTSTAMP:".$this->getStamp()."\n";
        $event .= "UID:".$this->m_uid."\n";
        $event .= "LOCATION:".$this->m_location."\n";
        $event .= "SUMMARY:".$this->m_summary."\n";
        $event .= "DESCRIPTION:".$this->m_description."\n";
        $event .= "END:VEVENT\n";
        
        return $event;
    }
    
    public function formatDate(\DateTime $datetime)
    {
        return $datetime->format("Ymd\THis");
    }
    
    public function getStartTime()
    {
        return $this->formatDate($this->m_dtstart);
    }
    
    public function getEndTime()
    {
        return $this->formatDate($this->m_dtend);
    }

	public function getTimeZone()
	{
		return $this->m_tzid->getName();
	}
	
	public function getStamp()
	{
		return $this->formatDate($this->m_dtstamp);
	}
}
